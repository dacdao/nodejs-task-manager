const express= require('express')
require('./db/mongoose')
const userRouter = require('./routers/user.js'
)
const taskRouter = require('./routers/task.js'
)
const app = express()

const port = process.env.PORT
// app.use((req, res, next)=>{
//   // console.log(req.method, req.path);
//   // next()
//
//   if(req.method==='GET'){
//     res.send('Get request disable')
//   }else{
//     next()
//   }
// })

// app.use((req, res, next)=>{
//   if(req.method){
//     res.send('under construction')
//   }else{
//     next()
//   }
// })

// const multer = require('multer')
// const upload = multer({
//   dest: 'images',
//   limits: {
//     fileSize: 1000000
//   },
//   // cb is callback
//   fileFilter(req, file, cb) {
//
//     // if (!file.originalname.endsWith('.pdf'))
//
//     // this uses the regex expression to find the end of the file that end in either .doc or .docx file
//     if(!file.originalname.match(/\.(doc|docx)$/))
//     {
//       return cb(new Error('please upload word document'))
//     }
//     cb(undefined, true)
//     // cb(new Error('file must be a word doc'))
//     // cb(undefined, true)
//
//   }
// })

// const errorMiddleware = (req, res, next)=>{
//  throw new Error('from my middleware')
//}

// app.post('/upload',upload.single('upload'),  (req,res)=>{
//   res.send()
// }, (error, req, res, next)=>{
//   res.status(400).send({error: error.message})
// })

app.use(express.json())
app.use(userRouter)
app.use(taskRouter)

// without middleware : new request -> run route handler

// with middleware: new request -> do something -> run route number


app.listen(port, ()=>{
  console.log('server is up on port ' + port)
})


// const bcrypt = require('bcryptjs')

// const myFunction = async () =>{
//   const password = 'abc123'
//   const hashedPassword = await bcrypt.hash(password, 8)
//
//   console.log(password);
//   console.log(hashedPassword)
//
//   const isMatch = await bcrypt.compare('123abc', hashedPassword)
//   console.log(isMatch);
// }
//
// myFunction()

// const jwt = require('jsonwebtoken')

// const myFunction = async ()=>{
//   const token = jwt.sign({_id:'abc123'}, 'newusertoken', {expiresIn: '0 second'})
//   console.log(token);
//
//   const data = jwt.verify(token, 'newusertoken')
//   console.log(data);
// }
//
// myFunction()



// const pet = {
//   name: 'hal'
// }
//
// pet.toJSON = function () {
//
//   return {}
// }
// console.log(JSON.stringify(pet));


// const Task = require('./models/task')
// const User = require('./models/user')
// const main = async () =>{
//   // const task = await Task.findById('5cd10985f84b8b34d06a6907')
//   // await task.populate('owner').execPopulate()
//   // console.log(task.owner)
//
//   const user = await User.findById('5cd109548316b9083053da85')
//   await user.populate('tasks').execPopulate()
//   console.log(user.tasks);
// }
//
// main()
