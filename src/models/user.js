const mongoose = require('mongoose')
const validator = require('validator')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const Task = require('./task')

const userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true
  },
  email:{
    type:String,
    required: true,
    trim: true,
    lowercase:true,
    unique: true,
    validate(value){
      if(!validator.isEmail(value)){
        throw new Error('email is invalid')
      }
    }
  },
  age: {
    type: Number,
    default: 0,
    validate(value) {
      if (value < 0){
        throw new Error('age must be positive')
      }
    }
  },
  tokens:[{
    token:{
      type: String,
      requred: true
    }
  }],
  avatar: {
    type: Buffer
  },
  password: {
    type: String,
    required: true,
    trim: true,
    minlength: 6,
    validate(value) {
      if (value.toLowerCase().includes('password')){
        throw new Error('you can"t have your password be password')
      }
    }
  }
}, {
  timestamps: true
})


userSchema.virtual('tasks', {
  ref: 'Task',
  localField: '_id',
  foreignField:'owner'
})


userSchema.methods.generateAuthToken = async function () {
  const user = this

  const token =jwt.sign({_id: user._id.toString()}, process.env.JWT_SECRECT)

  user.tokens=user.tokens.concat({token: token})

  await user.save()
  return token
}

userSchema.methods.toJSON = function (){
  const user = this
  const userObject = user.toObject()
  delete userObject.password
  delete userObject.tokens
  delete userObject.avatar
  return userObject
}

userSchema.statics.findByCred = async (email, password) =>{
  const user = await User.findOne({email: email})

  if (!user){
    throw new Error('unable to log in')
  }

  const isMatch = await bcrypt.compare(password, user.password)

  if (!isMatch){
    throw new Error('unable to log in')
  }

  return user
}

// hash plain text password
userSchema.pre('save', async function (next) {
  const user = this

  if (user.isModified('password')) {
    user.password = await bcrypt.hash(user.password, 8)
  }

  next()
})

//delete user tasks when user is removed
userSchema.pre('remove', async function (next){
  const user = this
  await Task.deleteMany({ owner: user._id})
  next()
})
const User = mongoose.model('User', userSchema)


module.exports = User
