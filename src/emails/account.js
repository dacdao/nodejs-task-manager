const sgMail = require('@sendgrid/mail')


// const sendgridAPIKey = ''

sgMail.setApiKey(process.env.SENDGRID_API_KEY)

const sendWelcomeEmail = (email, name)=>{
  sgMail.send({
    to: email,
    from: 'dacdao01@gmail.com',
    subject: 'Welcome to the app',
    text: `Welcome to the app, ${name}. Let me know if this app is useful`
    //html: can be use to create an email html styling
  })
}

const sendDeleteEmail = (email, name)=>{
  sgMail.send({
    to: email,
    from: 'dacdao01@gmail.com',
    subject: 'Sad that you are leaving',
    text: `Hi ${name}, \n
    I'm sad that you are leaving, please let me know why that is and we will try to help you.`
    //html: can be use to create an email html styling
  })
}
module.exports ={
  sendWelcomeEmail,
  sendDeleteEmail
}
