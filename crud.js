// This is the update functionality with promises included.

    db.collection('users').updateOne({
      _id: new ObjectID("5cc525214b5f721a6072d278")
    }, {
      $inc:{
        age: 1
      }
    }).then((result)=>{
    console.log(result)
}).catch((error) =>{
    console.log(error)
})

    db.collection('tasks').updateMany({
        completed: false
    }, {
        $set: {
            completed: true
        }
    }).then((result)=>{
        console.log(result)
    }).catch((error)=>{
        console.log(error)
    })



//This is the read part of CRUD, find functionality will find what you are looking for within the database.

  db.collection('users').findOne({ _id: new ObjectID("5c1113239cbfe605241f9071") }, (error, user) => {
      if (error) {
          return console.log('Unable to fetch')
      }

      console.log(user)
  })

  db.collection('users').find({ age: 30 }).toArray((error, users) => {
      console.log(users)
  })

  db.collection('tasks').findOne({ _id: new ObjectID("5c0fec243ef6bdfbe1d62e2f") }, (error, task) => {
      console.log(task)
  })

  db.collection('tasks').find({ completed: false }).toArray((error, tasks) => {
      console.log(tasks)
  })


  // below is the method in inserting datas into the collection. This is the create part of CRUD

    db.collection('users').insertOne({
      name: 'stark',
      age: '11'
    }, (error, result)=>{
      if(error) {
        return console.log('unable to insert user')
      }
      console.log(result.ops)
    })


  db.collection('users').insertMany([
    {
    name:'thao',
    age: 28
  },
  {
    name:'sexy boy',
    age:'1'
  }
  ],
    (error, result)=>{
    if (error){
      return console.log('unable to insert')
    }
    console.log(result.ops)
  })
  db.collection('tasks').insertMany([
    {
      description:"homework",
      completed:true
    },
    {
      description:'sleep',
      completed:true
    },
    {
      description:'eat',
      completed:false
    }
  ], (error, result)=>{
    if(error) {
      return console.log('unable to insert');
    }
    console.log(result.ops)
  })


//this is the delete function and the last part of the CRUD programming.
  db.collection('users').deleteMany({
        age: 11
    }).then((result)=>{
        console.log(result);
    }).catch((error)=>{
        console.log(error);
    })

    db.collection('tasks').deleteOne({
        description:"homework"
    }).then((result)=>{
        console.log(result);
    }).catch((error)=>{
        console.log(error);
    })
