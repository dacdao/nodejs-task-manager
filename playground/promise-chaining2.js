require('../src/db/mongoose.js')


const Task = require('../src/models/task.js')

//5ccb80e12dd53d30c080d3ce

Task.findByIdAndDelete('5ccb7cda75e7463014c0665c').then((task)=>{
  console.log(task)
  return Task.countDocuments({})
}).then((result)=>{
  console.log(result);
}).catch((e)=>{
  console.log(e)
})

const deleteTaskAndCount = async (id)=>{
  const task = await Task.findByIdAndDelete(id)
  const count = await Task.countDocuments({})
  return count
}

deleteTaskAndCount('5cccc7ee9c570a32c4ec5803').then((count)=>{
  console.log(count);
}).catch((e)=>{
  console.log(e);
})
