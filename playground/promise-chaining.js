require('../src/db/mongoose.js')


const User = require('../src/models/user.js')

//5ccb80e12dd53d30c080d3ce

User.findByIdAndUpdate('5ccbe8269719f743a4c7412c', { age: 1
}).then((user)=>{
  console.log(user)
  return User.countDocuments({age: 1})
}).then((result)=>{
  console.log(result);
}).catch((e)=>{
  console.log(e)
})


const updateAgeAndCount = async (id, age)=>{
  const user = await User.findByIdAndUpdate(id, {age: age})
  const count = await User.countDocuments({age: age})
  return count
}

updateAgeAndCount('5ccbe8269719f743a4c7412c', 2).then((count)=>{
 console.log(count);
})
.catch((e)=>{
  console.log(e);
})
